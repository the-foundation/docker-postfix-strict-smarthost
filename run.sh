#!/bin/bash




test -e  /etc/ssl/private || mkdir /etc/ssl/private
test -e  /etc/ssl/certs   || mkdir /etc/ssl/certs
mkdir /scripts &

while (true);do update-ca-certificates ;sleep 864000;done &

SYSTEM_CERT=/etc/ssl/certs/ssl-cert-snakeoil.pem
PRIVATE_KEY=/etc/ssl/private/ssl-cert-snakeoil.key

test -e  /etc/ssl/certs/ssl-cert-snakeoil.pem ||   (( echo "XX";echo "Heaven";echo "Web";echo "Internet Thingy" ;echo "TheInterNet";hostname -f;echo;echo;echo;echo;echo;echo;echo;echo)|openssl req -new -x509 -days 32768 -nodes -newkey rsa:4096 -keyout ${PRIVATE_KEY} -out ${SYSTEM_CERT}  ) 


(
##queue settings 
echo "maximal_queue_lifetime = 1h"
echo "bounce_queue_lifetime = 1h"
echo "maximal_backoff_time = 15m"
echo "minimal_backoff_time = 5m"


#set hostname
echo "myhostname = ${MAILNAME}"

# Enable auth
echo "smtp_sasl_auth_enable = yes"

# Set username and password
echo "smtp_sasl_password_maps = static:${RELAY_USERNAME}:${RELAY_PASSWORD}"
echo "smtp_sasl_security_options = noanonymous"
# Turn on tls encryption 
[[ "$STARTTLS" == "true" ]] && echo "smtp_tls_security_level = encrypt"
[[ "$STARTTLS" == "true" ]] && echo "smtp_use_tls = yes"


echo '# SMTPD TLS configuration for inbound connections'
echo 'smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem'
echo 'smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key'
echo 'smtpd_tls_protocols = !SSLv2, !SSLv3'
echo '# Enable Opportunistic TLS'
echo 'smtpd_tls_security_level = may'

echo '## only authed via tls'
echo "smtpd_tls_auth_only = yes"
echo "smtpd_tls_loglevel = 2"
echo 'smtp_tls_note_starttls_offer = yes'
echo "# displays TLS information in the E-Mail header"
echo "smtpd_tls_received_header = yes"
echo 'smtpd_tls_session_cache_database = lmdb:${data_directory}/smtpd_scache'
echo 'smtpd_use_tls  = yes'
echo 'smtpd_tls_session_cache_timeout = 3600s'
echo 'tls_random_source = dev:/dev/urandom'
echo 'tls_random_prng_update_period = 3600s'

echo "header_size_limit = 4096000"
# Set external SMTP relay host here IP or hostname accepted along with a port number. 
echo "relayhost = ${RELAY_HOST}:${RELAY_PORT}"
# accept email from our web-server only 
#echo "inet_interfaces = ${RELAY_NETS}"
echo "mynetworks = ${RELAY_NETWORKS}"

# Forcing the from address when postfix relays over smtp
echo "sender_canonical_classes = envelope_sender, header_sender"
echo "sender_canonical_maps =  regexp:/etc/postfix/sender_canonical_maps"
echo "header_checks = regexp:/etc/postfix/header_checks"
echo "maillog_file = /dev/stdout"
) >> /etc/postfix/main.cf

echo '/.+/    '${RELAY_MAILFROM} >> /etc/postfix/sender_canonical_maps

echo '/From:.*/ REPLACE From: '${RELAY_MAILFROM} >> /etc/postfix/header_checks

postmap /etc/postfix/sender_canonical_maps

postmap /etc/postfix/header_checks

newaliases


echo running
#postfix -v
#postfix start-fg
chown -R postfix /var/spool/postfix ||true &
chgrp -R postfix /var/spool/postfix ||true &


#postfix as client
postconf -e "smtp_use_tls = yes" "smtp_tls_security_level = encrypt" "smtp_tls_note_starttls_offer = yes" "smtp_sasl_auth_enable = yes" "smtp_sasl_security_options = noanonymous"
#postfix as server
postconf -e smtpd_sasl_security_options=noanonymous,noplaintext smtpd_sasl_tls_security_options=noanonymous

have_smtp_cred="no"
[[ -z "$SMTP_PASSWORD" ]] || [[ -z "$SMTP_DOMAIN" ]] || [[ -z "$SMTP_DOMAIN" ]] || have_smtp_cred="yes"

[[  "$have_smtp_cred" = "yes" ]] && { 
#### set up sasl password auth
    test -e /etc/sasl2||mkdir /etc/sasl2;(echo "pwcheck_method: auxprop" ;echo "auxprop_plugin: sasldb" ;echo "mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5 NTLM") >/etc/sasl2/smtpd.conf
    postconf -e smtpd_sasl_auth_enable=yes broken_sasl_auth_clients=yes smtpd_sasl_security_options=noanonymous,noplaintext smtpd_sasl_tls_security_options=noanonymous smtpd_relay_restrictions=permit_sasl_authenticated,reject_unauth_destination
    postconf -e "cyrus_sasl_config_path = /etc/sasl2, /etc/postfix/sasl/, /var/lib/sasl2"
    echo "###SASL:SPAWN##"    
#    while (true);do /usr/sbin/saslauthd -a sasldb -c -d ;sleep 5;done &
    (echo '#!/bin/sh'; 
    echo "ps -A|grep -v grep |grep saslauthd && killall -QUIT saslauthd"
    echo "/usr/sbin/saslauthd -a sasldb -c -d 2>&1  |sed  's/^/saslauthd: /g'|grep -e realm= -e service= -e response:" )> /scripts/saslstart.sh
    chmod +x /scripts/saslstart.sh

    test -e /etc/supervisord.conf || ( while (true);do /usr/sbin/saslauthd -a sasldb -c -d ;sleep 5;done ) &
    test -e /etc/supervisord.conf && grep saslauth /etc/supervisord.conf || (echo '[program:saslauthd]
command         = /scripts/saslstart.sh
autostart       = true
autorestart     = true
startsecs       = 5
stopwaitsecs    = 5
stdout_logfile  = /dev/stdout
stderr_logfile  = /dev/stderr
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0' >> /etc/supervisord.conf )
    echo "ADDING SMTP USER ${SMTP_USER}@${SMTP_DOMAIN}"
    echo "${SMTP_PASSWORD}" | saslpasswd2 -p -c -u "${SMTP_DOMAIN}" "${SMTP_USER}"
    ( echo '#!/bin/bash' ; echo ' read psw;echo "$psw" | saslpasswd2 -p -c -u ${1/*@/} ${1/@*/}') > /usr/bin/add_smtp_user
    chmod +x /usr/bin/add_smtp_user
    [[ -z "$SMTP_MORE_USERS" ]] || (
        #SMTP_MORE_USERS has to be like "username@domain.tld:USERsPASSWORD1273123 anottherone@sampledomain.lan:OIQ§O$IJQ§OIJQ§O$IJQ§O$IJQ§$"
        echo "ADDING_SMTP_CLIENT_USERS.."
        echo "$SMTP_MORE_USERS"|sed 's/ /\n/'|grep @|while read curline;do 
          myuser=${curline/:*/}
          mypass=${curline/*:/}
          echo "ADDING SMTP USER $myuser"
          echo "$mypass" | add_smtp_user $myuser
        done
    )
    chgrp postfix /etc/sasl2/sasldb2 /etc/sasl2 
    chmod g+r /etc/sasl2/sasldb2 /etc/sasl2 
    ( sleep 30 ; 
    echo "###SASL:TEST##"   ;  
    testsaslauthd -u "${SMTP_USER}" -r "${SMTP_DOMAIN}" -p "${SMTP_PASSWORD}" -s smtpd ) &

    echo   ; } ;

#nl /etc/postfix/master.cf
tail /etc/postfix/main.cf
#sed 's/^smtp\( \|\t\)/0.0.0.0:smtp /g' /etc/postfix/master.cf -i


/usr/sbin/postfix start-fg

