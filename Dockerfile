FROM alpine
RUN apk update && apk add --no-cache bash openssl ca-certificates  cyrus-sasl cyrus-sasl-crammd5 cyrus-sasl-digestmd5 cyrus-sasl-login cyrus-sasl-ntlm postfix postfix-stats curl pcre2 bash postfix-pcre
RUN update-ca-certificates
RUN ln -sf /dev/stdout /var/log/mail.log 
COPY run.sh /
CMD /bin/bash /run.sh


